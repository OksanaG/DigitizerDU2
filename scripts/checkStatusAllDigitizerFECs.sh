#!/bin/sh

# exit on any error
#set -e

function help {
    echo ""
    echo "Usage: checkStatusAllDigitizerFECs.sh"
    echo ""
    echo "This script will check the status property of Digitizer FEC's "
    echo ""
    exit 1;
}

if [[ "$1" == "-h" || "$1" == "--help" ]];
then
    help
fi

SCRIPT=$(readlink -f "$0")
PROJECT_PATH="$(dirname "$SCRIPT")"     # path where this script is located in
FEC_FOLDERS=/home/bel/schwinn/lnx/git/DigitizerDU2/src/test/*

# dal004, fel0013 --> development systems
# dal019, dal020 --> Monsterrack, these do not have scopes connected yet
# dal009, dal010, fel0024 --> HF Scopes, flowgraph not functional yet
IGNORE_FECS="dal004 dal009 dal010 dal019 dal020 fel0013 fel0024"

declare -a FECS

CHECK_STATUS_SCRIPT=$PROJECT_PATH/check-status-prop-fec.sh
for folder in $FEC_FOLDERS
do
    FEC=$(basename $folder)
    IGNORED=FALSE
    for IGNORE_FEC in $IGNORE_FECS
    do
      if [ "$FEC" = "$IGNORE_FEC" ]; then
        IGNORED=TRUE
      fi
    done
    if [ "$IGNORED" = "TRUE" ]; then
        #echo "ignoring  FEC $FEC"
        continue
    fi
    FECS+=($FEC)
done

for FEC in "${FECS[@]}"
do
    echo "checking FEC: $FEC"
    $CHECK_STATUS_SCRIPT $FEC -v
done

echo "done"
exit 0