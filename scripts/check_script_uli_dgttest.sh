#!/bin/sh
###############################################################################
##                                                                           ##
##        Shell script to test digitizer host computers via ping             ##
##                                                                           ##
##---------------------------------------------------------------------------##
## File:      dgttest.sh                                                     ##
## Author:    Ulrich Becker                                                  ##
## Date:      17.02.2020                                                     ##
## Copyright: GSI Helmholtz Centre for Heavy Ion Research GmbH               ##
###############################################################################
readonly ESC_BOLD="\e[1m"
readonly ESC_ERROR=${ESC_BOLD}"\e[31m"
readonly ESC_PASS=${ESC_BOLD}"\e[32m"
readonly ESC_WARN=${ESC_BOLD}"\e[33m"
readonly ESC_NORMAL="\e[0m"

readonly SERVICE_PORT_SUFFIX="i64"
readonly IP_SUFFIX=".acc.gsi.de"

die()
{
   echo -e $ESC_ERROR"ERROR: $@"$ESC_NORMAL 1>&2
   exit 1;
}

checkName()
{
   if [ ! -n "$1" ]
   then
      die "Missing argument e.g.: dal042 or fel4711"
   fi

   case ${1:0:3} in
      "fel")
         readonly local digits=4
      ;;
      "dal")
         readonly local digits=3
      ;;
      *)
         die "Name has to begin with \"dal\" or \"fel\" and not \"${1:0:3}\"!"
      ;;
   esac

   if [[ ${#1} != $(((3 + $digits))) ]]
   then
      die "Wrong length, $(((3 + $digits))) characters will expected!"
   fi

   if ! [[ $1 =~ [a-z]{3}[0-9]{$digits} ]]
   then
      die "Expecting $digits digits after \"${1:0:3}\"!"
   fi
}


readonly NAME=$(echo $1 | awk '{print tolower($0)}')

checkName $NAME

readonly MAIN_PORT=${NAME}${IP_SUFFIX}
readonly SERVICE_PORT=${NAME}${SERVICE_PORT_SUFFIX}${IP_SUFFIX}

RESULT_SERVICE=0
RESULT_MAIN=0

echo "Testing \"${SERVICE_PORT}\""
ping -c1 ${SERVICE_PORT} 2>/dev/null 1>/dev/null
if [ "$?" != '0' ]
then
  echo -e "${ESC_WARN}WARNING: Service interface of \"${NAME}\" does not answer!${ESC_NORMAL}" 1>&2
  RESULT_SERVICE=1
else
  echo "Service port of \"${NAME}\" is okay!"
fi

echo "Testing \"${MAIN_PORT}\""
ping -c1 ${MAIN_PORT} 2>/dev/null 1>/dev/null
if [ "$?" != '0' ]
then
   echo -e "${ESC_WARN}WARNING: Main interface of \"${NAME}\" does not answer!${ESC_NORMAL}" 1>&2
   RESULT_MAIN=1
else
   echo "Main port of \"${NAME}\" is okay!"
fi

if [ "$RESULT_MAIN" == "0" ] && [ "$RESULT_SERVICE" == "0" ]
then
  echo -e "${ESC_BOLD}Result of \"$NAME\": ${ESC_PASS}PASS!${ESC_NORMAL}"
  exit 0
fi

if [ "$RESULT_SERVICE" == "0" ]
then
   echo -e "${ESC_WARN}May be device \"${NAME}\" is down!${ESC_NORMAL}"
elif [ "$RESULT_MAIN" == "0" ]
then
   echo -e "${ESC_WARN}Service interface of \"${NAME}\" corrupt!${ESC_NORMAL}"
else
   echo -e "${ESC_WARN}May be device \"${NAME}\" not connected!${ESC_NORMAL}"
fi
echo -e "${ESC_BOLD}Result of \"$NAME\": ${ESC_ERROR}FAIL!${ESC_NORMAL}"
exit 1

#=================================== EOF ======================================
