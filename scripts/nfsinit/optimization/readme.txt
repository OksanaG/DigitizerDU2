This folder contains optization configs per FEC types.

E.g. the FEC-type "supermicro" runs gnuradio-modules which heavily use
vector-calculations via libvolk and fouier transformations via libfftw3.

The performance of these libraries can be speed up by the factor ~10 when
some tests are run to check the optimal CPU performance profile for different
possible calculation scenarios.

The result of these tests (which can take hours) is stored in a config file
which can be used on every identical FEC.

