#!/bin/sh

# exit on any error
#set -e

function help {
    echo ""
    echo "Usage: rebootAllDigitizerFECs.sh"
    echo ""
    echo "This script will do a hard reboot and fpga reset on all Digitizer FEC's while only giving the password once"
    echo ""
    exit 1;
}

if [[ "$1" == "-h" || "$1" == "--help" ]];
then
    help
fi

# Start ssh-agent and load keys, if not already started
if [ -z "$SSH_AUTH_SOCK" ] ; then
  eval `ssh-agent -s`
  ssh-add
fi

SCRIPT=$(readlink -f "$0")
PROJECT_PATH="$(dirname "$SCRIPT")"     # path where this script is located in
FEC_FOLDERS=/home/bel/schwinn/lnx/git/DigitizerDU2/src/test/*

# dal004, fel0013 --> development systems
# dal019, dal020 --> Monsterrack, these do not have scopes connected yet
# dal009, dal010, fel0024 --> HF Scopes, flowgraph not functional yet
IGNORE_FECS="dal004 dal009 dal010 dal019 dal020 fel0013 fel0024"

declare -a FECS

CHECK_STATUS_SCRIPT=$PROJECT_PATH/check-status-prop-fec.sh
for folder in $FEC_FOLDERS
do
    FEC=$(basename $folder)
    IGNORED=FALSE
    for IGNORE_FEC in $IGNORE_FECS
    do
      if [ "$FEC" = "$IGNORE_FEC" ]; then
        IGNORED=TRUE
      fi
    done
    if [ "$IGNORED" = "TRUE" ]; then
        #echo "ignoring  FEC $FEC"
        continue
    fi
    # releaseDigitizer.sh $FEC 4.1.0
    echo "resetting FEC $FEC"
    soft_reset_fec $FEC &> /dev/null
    FECS+=($FEC)
done

echo " ### rebooting the following FECS:"
echo ${FECS[@]}

$PROJECT_PATH/reboot-fecs.sh ${FECS[@]}

echo "### Reboot sent to all FEC'S now attempting to ping them ###"

TIMEOUT=10
$PROJECT_PATH/ping-fecs.sh -t $TIMEOUT ${FECS[@]}

echo "### Reboot of all FEC's finished sucessfully### "

exit 0
