#!/bin/sh

# Use -i to trigger instance file upload to DB

if [ $# -eq 0 ]
  then
    echo "Error: No arguments supplied. Call releaseDigitizer <FEC-Name> <DigitizerClass-Version>"
    exit 1
fi
if [ $# -lt 2 ]
  then
    echo "Error: Wrong number of arguments supplied. Call releaseDigitizer <FEC-Name> <DigitizerClass-Version> [-i]"
    exit 1
fi

if [ $# -gt 3 ]
  then
    echo "Error: Wrong number of arguments supplied. Call releaseDigitizer <FEC-Name> <DigitizerClass-Version> [-i]"
    exit 1
fi


FEC=$1
VERSION=$2
SCRIPT=$(readlink -f "$0")
PROJECT_PATH=$(dirname "$(dirname "$SCRIPT")")     # path where this script is located in

FEC_FOLDER=/common/export/fesa/local/$FEC/DigitizerDU2-$VERSION
FESA_VERSION=7.0.0

mkdir -p $FEC_FOLDER

## binray ##
cp -v $PROJECT_PATH/build/bin/x86_64/DigitizerDU2_M $FEC_FOLDER/DigitizerDU2

## fesa config files ##
cp -v $PROJECT_PATH/scripts/fesa.cfg $FEC_FOLDER
cp -v /common/export/fesa/global/etc/fesa/$FESA_VERSION/cmw.cfg $FEC_FOLDER
cp -v /common/export/fesa/global/etc/fesa/$FESA_VERSION/log.cfg $FEC_FOLDER
cp -v /common/export/fesa/global/etc/fesa/$FESA_VERSION/messages.cfg $FEC_FOLDER
cp -v /common/export/fesa/global/etc/fesa/$FESA_VERSION/messagesLab.cfg $FEC_FOLDER

## usb reset helper ##
cp -v $PROJECT_PATH/scripts/nfsinit/reset.usb /common/export/fesa/local/$FEC/
chmod +x /common/export/fesa/local/$FEC/reset.usb

## instance file ##
cp -v $PROJECT_PATH/src/test/$FEC/DeviceData_DigitizerDU2.instance $FEC_FOLDER/DigitizerDU2.instance

## start scripts ##
cp -v $PROJECT_PATH/src/test/$FEC/start* $FEC_FOLDER

## remove perisitancy file ##
rm /common/fesadata/data/$FEC/DigitizerDU2.$FECDigitizerClass2PersistentData.xml

## grc files ##
cp -v $PROJECT_PATH/src/test/$FEC/*.grc $FEC_FOLDER 2>/dev/null
#cp -v $PROJECT_PATH/grc/*.grc $FEC_FOLDER

if [ -f ${FEC_FOLDER}/${FEC}.grc ]; then
  if [ -L ${FEC_FOLDER}/flowgraph.grc ]; then
    unlink ${FEC_FOLDER}/flowgraph.grc
  fi
  ln -s ${FEC}.grc ${FEC_FOLDER}/flowgraph.grc
fi

## Timing Simulation ##
cp -v $PROJECT_PATH/scripts/inject_wr_timing/Inject_simulation*.sh $FEC_FOLDER
chmod +x $PROJECT_PATH/scripts/inject_wr_timing/Inject_simulation*.sh

# Update/Overwrite daemon nfsinit link
cd /common/export/nfsinit/${FEC}
if [ -L 50_fesa ]; then
    rm 50_fesa
fi
ln -fvs ../global/digitizer_fesa_environment 50_fesa

# Re-link the DeployUnit folders to newly installed folder
cd /common/export/fesa/local/${FEC}
if [ -L DigitizerDU2 ]; then
    unlink DigitizerDU2
fi
if [ -L DigitizerDU2-d ]; then
    unlink DigitizerDU2-d
fi

# If there is a folder "DigitizerDU2-d", we backup it
if [ -d DigitizerDU2-d ]; then
    mv DigitizerDU2-d DigitizerDU2-d.backup
fi

ln -s DigitizerDU2-$VERSION DigitizerDU2
ln -s DigitizerDU2-$VERSION DigitizerDU2-d

# Generate new FEX zip file and copy it to /common/usr/fesa/htdocs/fex
mkdir $PROJECT_PATH/src/test/$FEC/DigitizerClass2.$VERSION
cp $PROJECT_PATH/src/test/$FEC/DeviceData_DigitizerDU2.instance $PROJECT_PATH/src/test/$FEC/DigitizerClass2.$VERSION/$FEC.instance
cp $PROJECT_PATH/../DigitizerClass2/src/DigitizerClass2.design $PROJECT_PATH/src/test/$FEC/DigitizerClass2.$VERSION/DigitizerClass2.$VERSION.design
# 'ntgmNetworkNameneeds' to be in line 3 !
echo -e "\n\ntgmNetworkName=" > $PROJECT_PATH/src/test/$FEC/server.properties
cd $PROJECT_PATH/src/test/$FEC/
zip -r DigitizerDU2_$FEC.zip DigitizerClass2.$VERSION server.properties
rm -r $PROJECT_PATH/src/test/$FEC/DigitizerClass2.$VERSION
rm $PROJECT_PATH/src/test/$FEC/server.properties
cp -v $PROJECT_PATH/src/test/$FEC/DigitizerDU2_$FEC.zip /common/usr/fesa/htdocs/fex

## Only for development. For production, libraries and their dependencies should be released with:              ##
## gr-digitizers/createTarball.sh / gr-digitizers/createTarball_dependencies.sh / gr-flowgraph/createTarball.sh ##
#rm -vrf $FEC_FOLDER/libgnuradio-*
#cp -v /common/usr/cscofe/opt/gr-flowgraph/${VERSION}/lib64/libgnuradio-flowgraph-*.so.0.0.0 $FEC_FOLDER
#cp -v /common/usr/cscofe/opt/gr-digitizer/${VERSION}/lib64/libgnuradio-digitizers-*.so.0.0.0 $FEC_FOLDER

# Optionally update the Instance File in the DB, so that JAPC is happy
if [ $# -eq 3 ]; then
    if [[ "$3" == "-i" ]]; then
        patch $PROJECT_PATH/src/test/$FEC/DeviceData_DigitizerDU2.instance < $PROJECT_PATH/scripts/remove_command.diff
        # TODO: CUrrently bug in "fesa3 -e DeviceData_DigitizerDU2.instance" .. use that when fixed !
        eclipse-neon $PROJECT_PATH/src/test/$FEC/DeviceData_DigitizerDU2.instance
        read -n 1 -s -r -p "### Please upload the instance file to the DB now and press any key to continue ###"
        cd $PROJECT_PATH/src/test/$FEC
        git checkout -- DeviceData_DigitizerDU2.instance
        # some cleanup
        rm *.orig
        rm *.rej
        rm *.backup
    fi
fi

echo ""
echo "release complete"
